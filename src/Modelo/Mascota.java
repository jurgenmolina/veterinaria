/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 * 
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 */
public class Mascota {
    private int id;
    private String nombre;
    private String dueno;
    private String edad;
    private String raza;
    private String genero;
    private boolean vacunado;
    private boolean banado;

    public Mascota() {
    }

    public Mascota(int id, String nombre, String dueno, String edad, String raza, String genero, boolean vacunado, boolean banado) {
        this.id = id;
        this.nombre = nombre;
        this.dueno = dueno;
        this.edad = edad;
        this.raza = raza;
        this.genero = genero;
        this.vacunado = vacunado;
        this.banado = banado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getVacunado() {
        return vacunado;
    }

    public void setVacunado(boolean vacunado) {
        this.vacunado = vacunado;
    }

    public boolean getBanado() {
        return banado;
    }

    public void setBanado(boolean banado) {
        this.banado = banado;
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    
    
    
}
