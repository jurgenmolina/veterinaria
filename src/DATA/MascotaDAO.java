/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DATA;

import Modelo.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 */
public class MascotaDAO{

    public static final String SQL_GET_ALL = "SELECT * FROM mascota";
    private static final String SQL_CREATE = "INSERT INTO `mascota` (`id`, `nombre`, `dueno`, `edad`, `raza`, `genero`, `vacunado`, `banado`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE = "UPDATE `mascota` SET `nombre`=?, `dueno`=?,`edad`=?, `raza`=?,`genero`=?, `vacunado`=?, `banado`=? WHERE id=?";
    private static final String SQL_DELETE = "DELETE FROM `mascota` WHERE id=?";

    public List<Mascota> getList() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Mascota objeto;
        List<Mascota> lista = new ArrayList<>();

        try {
            con = ConexionBD.getConnection();
            ps = con.prepareStatement(this.SQL_GET_ALL);
            res = ps.executeQuery();
            while (res.next()) {


                objeto = new Mascota(
                        res.getInt("id"),
                        res.getString("nombre"),
                        res.getString("dueno"),
                        res.getString("edad"),
                        res.getString("raza"),
                        res.getString("genero"),
                        res.getBoolean("vacunado"),
                        res.getBoolean("banado")
                );

                lista.add(objeto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Mascota.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ConexionBD.close(res);
                ConexionBD.close(ps);
                ConexionBD.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(Mascota.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lista;
    }

    public int create(Mascota e) {
        return this.ejecutarSQL(e, 3);
    }

    public int update(Mascota e) {
        return this.ejecutarSQL(e, 2);
    }

    public int delete(Mascota e) {
        return this.ejecutarSQL(e, 1);
    }

    private int ejecutarSQL(Mascota e, int t) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = ConexionBD.getConnection();
            switch (t) {
                case 1: {
                    ps = con.prepareStatement(this.SQL_DELETE);
                    ps.setInt(1, e.getId());
                    break;
                }
                case 2: {
                    ps = con.prepareStatement(this.SQL_UPDATE);
                    ps.setString(1, e.getNombre());
                    ps.setString(2, e.getDueno());
                    ps.setString(3, e.getEdad());
                    ps.setString(4, e.getRaza());
                    ps.setString(5, e.getGenero());
                    ps.setBoolean(6, e.getVacunado());
                    ps.setBoolean(7, e.getBanado());
                    ps.setInt(8, e.getId());
                    break;
                }
                case 3: {
                    ps = con.prepareStatement(this.SQL_CREATE);
                    ps.setInt(1, e.getId());
                    ps.setString(2, e.getNombre());
                    ps.setString(3, e.getDueno());
                    ps.setString(4, e.getEdad());
                    ps.setString(5, e.getRaza());
                    ps.setString(6, e.getGenero());
                    ps.setBoolean(7, e.getVacunado());
                    ps.setBoolean(8, e.getBanado());
                    

                    break;
                }
                default:
                    break;
            }

            registros = ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Mascota.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(Mascota.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return registros;
    }
}
