/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DATA.MascotaDAO;
import Modelo.Mascota;
import Vista.IngresarMascota;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 */
public class MascotaControlador implements ActionListener {

    private Mascota mascota = new Mascota();
    private MascotaDAO mascotaDAO = new MascotaDAO();
    private IngresarMascota vista;

    public MascotaControlador() {
    }

    public MascotaControlador(Mascota mascota, MascotaDAO mascotaDAO, IngresarMascota vista) {
        this.mascota = mascota;
        this.mascotaDAO = mascotaDAO;
        this.vista = vista;
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnModificar.addActionListener(this);
        this.vista.btnEliminar.addActionListener(this);
    }


    public void iniciar() {
        vista.setTitle("Veterinaria");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    
    

    public void comboID() {
        vista.comboID.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        
        if (arg0.getSource() == vista.btnGuardar) {

             mascota.setId(0);
            mascota.setNombre(vista.txtNombre.getText());
            mascota.setDueno(vista.txtDueno.getText());
            mascota.setEdad(vista.txtEdad.getText());
            mascota.setRaza(vista.txtRaza.getText());
            mascota.setGenero(vista.comboGenero.getSelectedItem().toString());
            
            String vacunado = vista.comboVacunado.getSelectedItem().toString();
            mascota.setVacunado(vacunadoBoolean(vacunado));
            
            String banado = vista.comboBanado.getSelectedItem().toString();
            mascota.setBanado(banadoBoolean(banado));
            

            if (mascota.getNombre().isEmpty() || mascota.getDueno().isEmpty() ||
                    mascota.getEdad().isEmpty() || mascota.getRaza().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Complete los campos");
            } else {
                boolean confirmacion = duplicado(mascota.getNombre());

                if (confirmacion) {
                    JOptionPane.showMessageDialog(null, "Ya se encuentra registrado, verifique el nombre");
                } else {
                    mascotaDAO.create(mascota);
                    JOptionPane.showMessageDialog(null, "Ingresado con exito");
                    limpiar();
                }
            }

        }

        if (arg0.getSource() == vista.btnModificar) {

            int id = Integer.parseInt(vista.comboID.getSelectedItem().toString());
            mascota.setId(id);
            mascota.setNombre(vista.txtNombre.getText());
            mascota.setDueno(vista.txtDueno.getText());
            mascota.setEdad(vista.txtEdad.getText());
            mascota.setRaza(vista.txtRaza.getText());
            mascota.setGenero(vista.comboGenero.getSelectedItem().toString());
            
            String vacunado = vista.comboVacunado.getSelectedItem().toString();
            mascota.setVacunado(vacunadoBoolean(vacunado));
            
            String banado = vista.comboBanado.getSelectedItem().toString();
            mascota.setBanado(banadoBoolean(banado));
            
            

            if (mascota.getNombre().isEmpty() || mascota.getDueno().isEmpty() ||
                    mascota.getEdad().isEmpty() || mascota.getRaza().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Complete los campos");
            } else {
                mascotaDAO.update(mascota);
                JOptionPane.showMessageDialog(null, "Actualizado con exito");
                limpiar();
            }
        }

        if (arg0.getSource() == vista.btnEliminar) {
            int id = Integer.parseInt(vista.comboID.getSelectedItem().toString());

            mascota.setId(id);

            mascotaDAO.delete(mascota);
            JOptionPane.showMessageDialog(null, "Eliminado con exito");
            limpiar();
        }

        if (arg0.getSource() == vista.comboID) {

            List<Mascota> list = new ArrayList<>();
            list = mascotaDAO.getList();

            String opcion = (String) vista.comboID.getSelectedItem();
            int o = Integer.parseInt(opcion);

            if (!list.isEmpty()) {
                for (Mascota p : list) {
                    if (p.getId() == o) {
                        vista.txtNombre.setText(p.getNombre());
                        vista.txtDueno.setText(p.getDueno());
                        vista.txtEdad.setText(p.getEdad());
                        vista.txtRaza.setText(p.getRaza());
                        vista.comboGenero.setSelectedItem(p.getGenero());
                        
                        boolean x = p.getVacunado();
                        int i = 1;
                        if (x) {
                            i = 0;
                        }
                        vista.comboVacunado.setSelectedIndex(i);
                        
                        boolean z = p.getBanado();
                        int j = 1;
                        if (z) {
                            j = 0;
                        }
                        vista.comboBanado.setSelectedIndex(j);
                    }
                }
            }
        }


    }

    public void limpiar() {

        vista.txtEdad.setText("");
        vista.txtNombre.setText("");
        vista.txtDueno.setText("");
        vista.txtRaza.setText("");

    }

    public void llenarModeloComboBox() {

        List<Mascota> list = new ArrayList<>();
        list = mascotaDAO.getList();

        if (!list.isEmpty()) {

            for (int i = 0; i < list.size(); i++) {
                this.vista.comboID.addItem(String.valueOf(list.get(i).getId()));
            }
            return;
        }
        this.vista.comboID.addItem("vacio");
    }

    private boolean duplicado(String nombre) {

        List<Mascota> list = new ArrayList<>();
        list = mascotaDAO.getList();
        boolean confirmacion = false;

        for (Mascota c : list) {
            if (c.getNombre().equals(nombre)) {
                confirmacion = true;
            }
        }
        return confirmacion;

    }

    private boolean isNumero(int dato) {

        String cadena = String.valueOf(dato);

        if (cadena.isEmpty()) {
            return true;
        }

        return false;
    }
    
     private boolean vacunadoBoolean(String texto) {

        boolean valida = false;
        if (texto.equals("SI")) {
            valida = true;
        }

        return valida;
    }
     
     private boolean banadoBoolean(String texto) {

        boolean valida = false;
        if (texto.equals("SI")) {
            valida = true;
        }

        return valida;
    }
}
