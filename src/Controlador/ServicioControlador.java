/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DATA.MascotaDAO;
import Modelo.Mascota;
import Vista.Servicios;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Jurgenmolina <jurgenmolina29@gmail.com>
 */
public class ServicioControlador implements ActionListener {

    private Mascota mascota = new Mascota();
    private MascotaDAO mascotaDAO = new MascotaDAO();
    private Servicios vista;

    public ServicioControlador() {
    }

    public ServicioControlador(Mascota mascota, MascotaDAO mascotaDAO, Servicios vista) {
        this.mascota = mascota;
        this.mascotaDAO = mascotaDAO;
        this.vista = vista;
        this.vista.btnVacunar.addActionListener(this);
        this.vista.btnBanar.addActionListener(this);
    }


    public void iniciar() {
        vista.setTitle("Veterinaria");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    
    

    public void comboID() {
        vista.comboID.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        
        if (arg0.getSource() == vista.btnBanar) {

            int id = Integer.parseInt(vista.comboID.getSelectedItem().toString());
        
        Mascota d = banarMascota(id);
        
        int precio = razaMascota(d.getRaza());
        
        mascotaDAO.update(d);
        JOptionPane.showMessageDialog(null, "HEMOS BAÑADO TU MASCOTA!, Costo: " + precio);
            

        }
        
        if (arg0.getSource() == vista.btnVacunar) {

            int id = Integer.parseInt(vista.comboID.getSelectedItem().toString());
        
        Mascota d = vacunarMascota(id);
        
        
        
        mascotaDAO.update(d);
        JOptionPane.showMessageDialog(null, "VACUNAMOS TU MASCOTA!, Valor: 10000" );
            

        }

        

        if (arg0.getSource() == vista.comboID) {

            List<Mascota> list = new ArrayList<>();
            list = mascotaDAO.getList();

            String opcion = (String) vista.comboID.getSelectedItem();
            int o = Integer.parseInt(opcion);

            if (!list.isEmpty()) {
                for (Mascota p : list) {
                    if (p.getId() == o) {
                        vista.txtNombre.setText(p.getNombre());
                        
                        
                    }
                }
            }
        }


    }

    public void limpiar() {

        vista.txtNombre.setText("");

    }

    public void llenarModeloComboBox() {

        List<Mascota> list = new ArrayList<>();
        list = mascotaDAO.getList();

        if (!list.isEmpty()) {

            for (int i = 0; i < list.size(); i++) {
                this.vista.comboID.addItem(String.valueOf(list.get(i).getId()));
            }
            return;
        }
        this.vista.comboID.addItem("vacio");
    }

    public Mascota banarMascota(int id) {
        
        List<Mascota> mascotas = new ArrayList<>();
        mascotas = mascotaDAO.getList();
        
        for (Mascota mascota : mascotas) {
            if (mascota.getId() == (id)) {
                mascota.setBanado(true);
                return mascota;
            }
        }
        
        return null;
        
    }
    
    public Mascota vacunarMascota(int id) {
        
        List<Mascota> mascotas = new ArrayList<>();
        mascotas = mascotaDAO.getList();
        
        for (Mascota mascota : mascotas) {
            if (mascota.getId() == (id)) {
                mascota.setVacunado(true);
                return mascota;
            }
        }
        
        return null;
        
    }
    
    public void llenarTabla() {
        
        List<Mascota> list = new ArrayList<>();
        list = mascotaDAO.getList();
        
        String[] info = new String[5];

        if (!list.isEmpty()) {
            
            for (int i = 0; i < list.size(); i++) {
                
                info[0] = String.valueOf(list.get(i).getId());
                info[1] = list.get(i).getNombre();
                String vacunado = "NO";
                if (list.get(i).getVacunado()) {
                    vacunado = "SI";
                }
                
                String banado = "NO";
                if (list.get(i).getBanado()) {
                    banado = "SI";
                }
                info[2] = list.get(i).getRaza();
                info[3] = vacunado;
                info[4] = banado;
                
                vista.modelo.addRow(info);
            }
        }
    }
    
    public int razaMascota(String raza) {
        
        if (raza.equalsIgnoreCase("Pincher") || raza.equalsIgnoreCase("mini puf")
                || raza.equalsIgnoreCase("Otro")) {
            return 10000;
        }
        
        if (raza.equalsIgnoreCase("Caniche")) {
            return 15000;
        }
        
        if (raza.equalsIgnoreCase("Beagle")) {
            return 30000;
        }
        
        return 10000;
        
    }

}
